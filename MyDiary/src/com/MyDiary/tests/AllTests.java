package com.MyDiary.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({com.MyDiary.model.tests.NoteTest.class, 
	com.MyDiary.view.presentationModel.tests.NotePresentationModelTest.class,
	com.MyDiary.infra.persistence.tests.NoteSQLiteDBPersistorTest.class})
public class AllTests {

}
