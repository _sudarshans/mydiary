package com.MyDiary.datatransfer;

import java.util.HashMap;
import java.util.Map;

import com.MyDiary.datatransfer.interfaces.NoteRequest;

public class NoteRequestImpl implements NoteRequest {

	private final Map<Object, Object> _requestAttributes;
	public NoteRequestImpl() {
		_requestAttributes = new HashMap<Object, Object>();
	}
	
	@Override
	public void setAttribute(Object key, Object value) {
		// TODO Auto-generated method stub
		_requestAttributes.put(key, value);
	}

	@Override
	public Object getAttribute(Object key) {
		// TODO Auto-generated method stub
		return _requestAttributes.get(key);
	}

}
