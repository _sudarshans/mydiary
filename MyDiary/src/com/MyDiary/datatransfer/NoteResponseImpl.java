package com.MyDiary.datatransfer;

import java.util.HashMap;
import java.util.Map;

import com.MyDiary.datatransfer.interfaces.NoteResponse;

public class NoteResponseImpl implements NoteResponse {

	private final Map<Object, Object> _responseAttributes;
	public NoteResponseImpl() {
		_responseAttributes = new HashMap<Object, Object>();
	}
	
	@Override
	public void setAttribute(Object key, Object value) {
		// TODO Auto-generated method stub
		_responseAttributes.put(key, value);
	}

	@Override
	public Object getAttribute(Object key) {
		// TODO Auto-generated method stub
		return _responseAttributes.get(key);
	}

}
