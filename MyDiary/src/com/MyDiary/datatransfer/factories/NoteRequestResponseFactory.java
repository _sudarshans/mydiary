package com.MyDiary.datatransfer.factories;

import com.MyDiary.datatransfer.NoteRequestImpl;
import com.MyDiary.datatransfer.NoteResponseImpl;
import com.MyDiary.datatransfer.interfaces.NoteRequest;
import com.MyDiary.datatransfer.interfaces.NoteResponse;

public class NoteRequestResponseFactory {

	private NoteRequestResponseFactory() {
		
	}
	private static final NoteRequestResponseFactory _instance = new NoteRequestResponseFactory();
	
	public static NoteRequestResponseFactory getInstance() {
		return _instance;
	}
	
	public NoteRequest getNoteRequest() {
		return new NoteRequestImpl();
	}

	public NoteResponse getNoteResonse() {
		return new NoteResponseImpl();
	}

}
