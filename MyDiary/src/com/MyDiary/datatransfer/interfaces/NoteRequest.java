package com.MyDiary.datatransfer.interfaces;

public interface NoteRequest {

	void setAttribute(Object key, Object value);
	
	Object getAttribute(Object key);
}
