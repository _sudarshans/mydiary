package com.MyDiary.datatransfer.interfaces;

public interface NoteResponse {
	void setAttribute(Object key, Object value);
	
	Object getAttribute(Object key);
}
