package com.MyDiary.datatransfer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.NoteData;

public class NoteDTO implements Comparable<NoteDTO>{

	private String _id;
	private String _content;
	private String _creationTime;
	private final String _systemDateFormat = "yyyy-MMM-dd' 'HH:mm:ss";
	private boolean _isPersisted = false;

	public NoteDTO(String id, String content, String creationTime) throws DiaryException {
		if (id == null) {
			throw new DiaryException("Illegal Argument : id cannot be null.");
		}
		this._id = id;
		this._content = content;
		this._creationTime = creationTime;
		this._isPersisted = true;
	}
	
	public NoteDTO(String content, String timeStamp) {
		this._content = content;
		this._creationTime = timeStamp;
	}

	public boolean isPersisted() {
		return _isPersisted;
	}

	public String getId() {
		return _id;
	}

	public String getContent() {
		return _content;
	}

	public String getCreationTime() {
		return _creationTime;
	}
	
	public Map<NoteData,String> getAll() {
		Map<NoteData,String> allInfo = new HashMap<NoteData, String>();
		allInfo.put(NoteData.ID, _id);
		allInfo.put(NoteData.CONTENT, _content);
		allInfo.put(NoteData.CREATION_TIME, _creationTime);
		return allInfo;
	}
	
	public int compareTo(NoteDTO inputNote) {

		String inputNoteDate = inputNote.getCreationTime();//inputNote.getInfoMap().get("noteCreationTime");
		String thisNoteDate = this._creationTime;//this.getInfoMap().get("noteCreationTime");
		DateFormat dateFormat = new SimpleDateFormat(_systemDateFormat);
		int result = 0;
		try {
			result = dateFormat.parse(thisNoteDate).compareTo(dateFormat.parse(inputNoteDate));
			if (result == 1) {
				result = -1;
			} else if (result == -1) {
				result = 1;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
}
