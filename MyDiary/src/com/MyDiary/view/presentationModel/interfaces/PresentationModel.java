package com.MyDiary.view.presentationModel.interfaces;

import java.util.Map;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.NoteViewData;

public interface PresentationModel {

	void update(Map<NoteViewData, String> viewMap) throws DiaryException;
	void syncView();
}
