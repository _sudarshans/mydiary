package com.MyDiary.view.presentationModel.tests;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.NoteViewData;
import com.MyDiary.view.presentationModel.NotePresentationModel;

public class NotePresentationModelTest {
	
	@Test
	public void testUpdate_NewClicked() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.NEW_CLICKED, null);
		notePModel.update(viewMap);
		
		assertTrue("Note content is not shown when new is clicked", notePModel.isNoteContentShown());
		assertTrue("Save Button is not shown when new is clicked", notePModel.isSaveButtonShown());
		assertTrue("Cancel button is not shown when new is clicked", notePModel.isCancelButtonShown());
	}

	@Test(expected = DiaryException.class)
	public void testUpdate_SearchClickedWithoutCriteria() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SEARCH_CLICKED, null);
		viewMap.put(NoteViewData.SEARCH_CONTENT, null);
		notePModel.update(viewMap);		
	}
	
	@Test
	public void testUpdate_SearchClickedWithCriteria() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SEARCH_CLICKED, null);
		viewMap.put(NoteViewData.SEARCH_CONTENT, "testNote");
		notePModel.update(viewMap);
		assertTrue("Search result not shown", notePModel.isSearchResultShown());
	}
	
	@Test(expected = DiaryException.class)
	public void testUpdate_SearchResultClicked_NullID() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SEARCH_RESULTITEM_CLICKED, null);
		notePModel.update(viewMap);		
	}
	
	@Test(expected = DiaryException.class)
	public void testUpdate_SearchResultClicked_BlankID() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SEARCH_RESULTITEM_CLICKED, "");
		notePModel.update(viewMap);		
	}
	
	@Test
	public void testUpdate_SearchResultClicked_ValidID() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SEARCH_RESULTITEM_CLICKED, "validId");
		notePModel.update(viewMap);	
		assertTrue("Note view is not shown.", notePModel.isNoteViewShown());
		assertTrue("Done is not shown.", notePModel.isDoneButtonShown());
	}
	
	@Test
	public void textUpdate_DoneClickedOnNoteView() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.DONE_CLICKED, null);
		notePModel.update(viewMap);
		assertTrue("Today's notes are not shown.", notePModel.isSearchResultShown());
	}
	
	@Test
	public void testUpdate_SaveClicked_WithNoteContent() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SAVE_CLICKED, null);
		viewMap.put(NoteViewData.NOTE_TEXT_CONTENT, "Note Content");
		notePModel.update(viewMap);	
		assertTrue("Today's notes are not shown.", notePModel.isSearchResultShown());
	}
	
	@Test(expected = DiaryException.class)
	public void testUpdate_SaveClicked_WithBlankNoteContent() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SAVE_CLICKED, null);
		viewMap.put(NoteViewData.NOTE_TEXT_CONTENT, "");
		notePModel.update(viewMap);	
	}
	
	@Test(expected = DiaryException.class)
	public void testUpdate_SaveClicked_WithNullNoteContent() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.SAVE_CLICKED, null);
		viewMap.put(NoteViewData.NOTE_TEXT_CONTENT, "");
		notePModel.update(viewMap);	
	}
	
	@Test
	public void testUpdate_CancelClicked_WithNoteContent() throws DiaryException {
		NotePresentationModel notePModel = new NotePresentationModel(false);
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.CANCEL_CLICKED, null);
		viewMap.put(NoteViewData.NOTE_TEXT_CONTENT, "Note Content");
		notePModel.update(viewMap);	
		assertTrue("Today's notes are not shown.", notePModel.isSearchResultShown());
	}
}
