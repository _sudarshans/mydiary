package com.MyDiary.view.presentationModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.MyDiary.datatransfer.factories.NoteRequestResponseFactory;
import com.MyDiary.datatransfer.interfaces.NoteRequest;
import com.MyDiary.datatransfer.interfaces.NoteResponse;
import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.NoteData;
import com.MyDiary.model.helpers.NoteController;
import com.MyDiary.view.NoteUIBroker;
import com.MyDiary.view.NoteViewData;
import com.MyDiary.view.presentationModel.interfaces.PresentationModel;

public class NotePresentationModel implements PresentationModel {

	private final boolean _syncUI;
	public NotePresentationModel(boolean sync) {
		_syncUI = sync;
		_broker = null;
	}
	public NotePresentationModel(NoteUIBroker noteSwingBroker) {
		_broker = noteSwingBroker;
		_syncUI = true;
	}
	private boolean _NoteContentShown = false;
	private boolean _SearchButtonShown = false;
	private boolean _SearchResultShown = false;
	private boolean _SaveButtonShown = false;
	private boolean _CancelButtonShown = false;
	private boolean _NoteViewShown = false;
	private boolean _DoneButtonShown = false;

	private boolean _SearchCriteriaShown = false;
	private boolean _NewButtonShown = false;
	private List<Map<NoteData,String>> noteResults = null;
	private final NoteUIBroker _broker;
	private final DateFormat _dateSearchFormat = new SimpleDateFormat("yyyy-MMM-dd");
	
	private final NoteController _noteController = NoteController.getInstance();
	private final NoteRequestResponseFactory _noteRequestResponseFactory = NoteRequestResponseFactory.getInstance();
	
	
	private Map<NoteData, String> _noteToView = null;

	private String getFormattedDate() {
		Date date = new Date();
		String formattedDate = _dateSearchFormat.format(date);	
		return formattedDate;
	}

	private void hideAll() {
		_NoteContentShown = false;
		_SearchButtonShown = false;
		_SearchResultShown = false;
		_SaveButtonShown = false;
		_CancelButtonShown = false;
		_NoteViewShown = false;
		_DoneButtonShown = false;
		_SearchCriteriaShown = false;
		_NewButtonShown = false;
	}

	private void performNewClicked() {
		hideAll();
		_NewButtonShown = true;
		_SearchCriteriaShown = true;
		_NoteContentShown = true;
		_SearchButtonShown = true;
		_SaveButtonShown = true;
		_CancelButtonShown = true;
		syncView();
	}

	private void saveNote(String noteContent) throws DiaryException {
		NoteRequest request = _noteRequestResponseFactory.getNoteRequest();
		request.setAttribute(NoteData.CONTENT, noteContent);
		_noteController.saveNote(request);
	}

	private void showNoteView(String noteId) throws DiaryException {
		hideAll();
		_NewButtonShown = true;
		_SearchCriteriaShown = true;
		_SearchButtonShown = true;
		_DoneButtonShown = true;
		_NoteViewShown = true;
		NoteRequest request = _noteRequestResponseFactory.getNoteRequest();
		request.setAttribute("searchCriteriaName", NoteData.ID);
		request.setAttribute("searchCriteriaValue", noteId);
		
		NoteResponse response = _noteController.getNote(request);
		_noteToView = (Map<NoteData, String>) response.getAttribute("searchResult");
		syncView();
	}

	private void showTodaysNotes() throws DiaryException {
		hideAll();
		_SearchResultShown = true;
		_NewButtonShown = true;
		_SearchCriteriaShown = true;
		_SearchButtonShown = true;
		String formattedDate = getFormattedDate();
		NoteRequest request = _noteRequestResponseFactory.getNoteRequest();
		request.setAttribute("searchCriteriaName", NoteData.CREATION_TIME);
		request.setAttribute("searchCriteriaValue", formattedDate);
		NoteResponse response = _noteController.findNotes(request);
		noteResults = (List<Map<NoteData, String>>) response.getAttribute("searchResults");
		syncView();
	}
	
	private void showSearchResults(String searchCriteria) throws DiaryException {
		hideAll();
		_SearchResultShown = true;
		_NewButtonShown = true;
		_SearchCriteriaShown = true;
		_SearchButtonShown = true;
		NoteRequest request = _noteRequestResponseFactory.getNoteRequest();
		request.setAttribute("searchCriteriaName", NoteData.ALL);
		request.setAttribute("searchCriteriaValue", searchCriteria);
//		noteResults = _noteController.findNotes(NoteData.ALL, searchCriteria);
//		noteResults = _noteController.findNotes(request);
		NoteResponse response = _noteController.findNotes(request);
		noteResults = (List<Map<NoteData, String>>) response.getAttribute("searchResults");
		syncView();	
	}

	public Map<NoteData, String> getNoteToView() {
		return _noteToView;
	}

	public List<Map<NoteData,String>> getNoteResults() {
		return noteResults;
	}

	public boolean isCancelButtonShown() {
		return _CancelButtonShown;
	}

	public boolean isDoneButtonShown() {
		return _DoneButtonShown;
	}

	public boolean isNoteContentShown() {
		return _NoteContentShown;
	}

	public boolean isNoteViewShown() {
		return _NoteViewShown;
	}

	public boolean isSaveButtonShown() {
		return _SaveButtonShown;
	}
	
	public boolean isSearchResultShown() {
		return _SearchResultShown;
	}
	
	public boolean isNewButtonShown() {
		return _NewButtonShown;
	}

	public boolean isSearchButtonShown() {
		return _SearchButtonShown;
	}
	
	public boolean isSearchCriteriaShown() {
		return _SearchCriteriaShown;
	}
	
	@Override
	public void syncView() {
		if (_syncUI) {
			_broker.updateUI(this);
		}
	}

	@Override
	public void update(Map<NoteViewData, String> viewMap) throws DiaryException {
		Set<NoteViewData> keySet = viewMap.keySet();
		Iterator<NoteViewData> keyIterator = keySet.iterator();
		
		while(keyIterator.hasNext()){
			switch(keyIterator.next()) {
			case SAVE_CLICKED:
				String noteContent = viewMap.get(NoteViewData.NOTE_TEXT_CONTENT);
				if (noteContent == null || "".equals(noteContent)) {
					throw new DiaryException("No content to save.");
				}
				saveNote(noteContent);
				showTodaysNotes();
				break;
			case NEW_CLICKED:
				performNewClicked();
				break;
			case SEARCH_CLICKED:
				String searchCriteria = viewMap.get(NoteViewData.SEARCH_CONTENT);
				if (searchCriteria == null || "".equals(searchCriteria)) {
					throw new DiaryException("No Search criteria.");
				}
				showSearchResults(searchCriteria);
				break;
			case SEARCH_RESULTITEM_CLICKED:
				String noteId = viewMap.get(NoteViewData.SEARCH_RESULTITEM_CLICKED);
				if (noteId == null || "".equals(noteId)) {
					throw new DiaryException("NoteId Should not be null.");
				}
				showNoteView(noteId);
				break;
			case CANCEL_CLICKED:
				showTodaysNotes();
				break;
			case SEARCH_RESULT_SHOWN:
				showTodaysNotes();
				break;
			case DONE_CLICKED:
				showTodaysNotes();
				break;
			default:
				break;
			}
		}
	}


}
