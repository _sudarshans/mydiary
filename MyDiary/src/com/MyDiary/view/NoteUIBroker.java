package com.MyDiary.view;

import java.util.Map;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.presentationModel.interfaces.PresentationModel;

public interface NoteUIBroker {

	void updatePresentationModel(Map<NoteViewData, String> viewMap) throws DiaryException;
	
	void updateUI(PresentationModel pModel);
}
