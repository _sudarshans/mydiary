package com.MyDiary.view;

import java.util.Map;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.presentationModel.NotePresentationModel;
import com.MyDiary.view.presentationModel.interfaces.PresentationModel;
import com.MyDiary.view.swing.layout.MyNotesUI;

public class NoteSwingBroker implements NoteUIBroker {
	
	private final PresentationModel _presentationModel;
	private final MyNotesUI _notesUI;

	public NoteSwingBroker(
			MyNotesUI _notesUI) {
		this._presentationModel = new NotePresentationModel(this);
		this._notesUI = _notesUI;
	}

	@Override
	public void updatePresentationModel(Map<NoteViewData, String> viewMap) throws DiaryException {
		_presentationModel.update(viewMap);
	}

	@Override
	public void updateUI(PresentationModel pModel) {
		_notesUI.update(pModel);
	}

}
