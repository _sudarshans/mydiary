package com.MyDiary.view.swing.layout.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.NoteUIBroker;
import com.MyDiary.view.NoteViewData;
import com.MyDiary.view.swing.layout.MyNotesUI;


public class NewButtonListener implements ActionListener {

	private MyNotesUI notesUI = null;
	public NewButtonListener(MyNotesUI myNotesUI) {
		this.notesUI = myNotesUI;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		NoteUIBroker broker = notesUI.getBroker();
		Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
		viewMap.put(NoteViewData.NEW_CLICKED, null);
		try {
			broker.updatePresentationModel(viewMap);
		} catch (DiaryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
