package com.MyDiary.view.swing.layout.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.JTextComponent;

public class FieldClearingMouseListener extends MouseAdapter {
	private static final List<String> defaultTextList = new ArrayList<String>();
	static {
		defaultTextList.add("Enter content");
		defaultTextList.add("Enter search criteria");
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
//		System.out.println("clicked");
		JTextComponent component = (JTextComponent)arg0.getComponent();
		if (defaultTextList.contains(component.getText())) {
			component.setText("");
		}

	}
}
