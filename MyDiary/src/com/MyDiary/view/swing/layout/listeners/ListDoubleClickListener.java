package com.MyDiary.view.swing.layout.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.view.NoteUIBroker;
import com.MyDiary.view.NoteViewData;
import com.MyDiary.view.swing.layout.MyNotesUI;

public class ListDoubleClickListener extends MouseAdapter {
	private MyNotesUI notesUI = null;

	public ListDoubleClickListener(MyNotesUI myNotesUI) {
		this.notesUI = myNotesUI;
	}

	public void mouseClicked(MouseEvent mouseEvent) {
		JTable table = (JTable) mouseEvent.getSource();
		if (mouseEvent.getClickCount() == 2) {
			String id = (String) table.getModel().getValueAt(
					table.getSelectedRow(), 0);

			NoteUIBroker broker = notesUI.getBroker();
			Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
			viewMap.put(NoteViewData.SEARCH_RESULTITEM_CLICKED, id);
			try {
				broker.updatePresentationModel(viewMap);
			} catch (DiaryException e1) {
				e1.printStackTrace();
			}
		}
	}

}
