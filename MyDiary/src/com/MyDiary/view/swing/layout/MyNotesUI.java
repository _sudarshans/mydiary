package com.MyDiary.view.swing.layout;


import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.NoteData;
import com.MyDiary.view.NoteSwingBroker;
import com.MyDiary.view.NoteUIBroker;
import com.MyDiary.view.NoteViewData;
import com.MyDiary.view.presentationModel.NotePresentationModel;
import com.MyDiary.view.presentationModel.interfaces.PresentationModel;
import com.MyDiary.view.swing.layout.listeners.CancelButtonActionListener;
import com.MyDiary.view.swing.layout.listeners.FieldClearingMouseListener;
import com.MyDiary.view.swing.layout.listeners.ListDoubleClickListener;
import com.MyDiary.view.swing.layout.listeners.NewButtonListener;
import com.MyDiary.view.swing.layout.listeners.SaveButtonActionListener;
import com.MyDiary.view.swing.layout.listeners.SearchButtonActionListener;
import com.MyDiary.view.swing.layout.listeners.ViewDoneButtonActionListener;


public class MyNotesUI {

	
	private final static int _NEW_BUTTON_INDEX = 0;
	private final static int _SEARCH_CRITERIA_FIELD_INDEX = 1;
	private final static int _SEARCH_BUTTON_INDEX = 2;
	private final static int _SEARCH_RESULT_PANEL_INDEX = 3;
	private final static int _NEW_NOTE_CONTENT_INDEX = 4;
	private final static int _SAVE_BUTTON_INDEX = 5;
	private final static int _CANCEL_BUTTON_INDEX = 6;
	private final static int _DONE_BUTTON_INDEX = 7;
	
	private JFrame frame = null;
	private final NoteUIBroker _broker;
	
	public NoteUIBroker getBroker() {
		return _broker;
	}

	public MyNotesUI() {
		_broker = new NoteSwingBroker(this);
	}

	public void addComponentsToPane(Container pane) {

		pane.setLayout(new GridBagLayout());

		JButton newNoteButton = new JButton("New");
		
		GridBagConstraints newButtonGBConstraints = new GridBagConstraints();
		newButtonGBConstraints.gridx = 0;
		newButtonGBConstraints.gridy = 0;

		JTextField searchCriteriaField = new JTextField("Enter search criteria");
		GridBagConstraints searchCritFieldGBConstraints = new GridBagConstraints();
		searchCritFieldGBConstraints.fill = GridBagConstraints.BOTH;
		searchCritFieldGBConstraints.weightx = 1.5;
		searchCritFieldGBConstraints.gridx = 1;
		searchCritFieldGBConstraints.gridy = 0;
		
		JButton searchButton = new JButton("Search");
		GridBagConstraints searchButtonGBConstraints = new GridBagConstraints();
		searchButtonGBConstraints.gridx = 2;
		searchButtonGBConstraints.gridy = 0;
		
		JPanel searchResultPanel = new JPanel();		
		
		JTextArea newNoteContentTextArea = new JTextArea("Enter content");
		GridBagConstraints contentAreaGBConstraints = new GridBagConstraints();
		contentAreaGBConstraints.fill = GridBagConstraints.BOTH;
		contentAreaGBConstraints.ipady = 40; // make this component tall
		contentAreaGBConstraints.weightx = 0.0;
		contentAreaGBConstraints.weighty = 1.0;
		contentAreaGBConstraints.gridwidth = 2;
		contentAreaGBConstraints.gridx = 1;
		contentAreaGBConstraints.gridy = 1;
		contentAreaGBConstraints.insets = new Insets(10, 0, 10, 10);

		JButton saveButton = new JButton("Save");
		GridBagConstraints saveButtonGBConstraints = new GridBagConstraints();
	
		saveButtonGBConstraints.fill = GridBagConstraints.NONE;
		saveButtonGBConstraints.gridx = 1;
		saveButtonGBConstraints.gridy = 2;
		saveButtonGBConstraints.anchor = GridBagConstraints.LAST_LINE_END;
		saveButtonGBConstraints.insets = new Insets(0, 0, 0, 5);
		
		JButton cancelButton = new JButton("Cancel");
		GridBagConstraints cancelButtonGBConstraints = new GridBagConstraints();
		cancelButtonGBConstraints.gridx = 2;
		cancelButtonGBConstraints.gridy = 2;
		
		JButton viewDoneButton = new JButton("Done");
		GridBagConstraints viewDoneButtonGBConstraints = new GridBagConstraints();
		viewDoneButtonGBConstraints.gridx = 2;
		viewDoneButtonGBConstraints.gridy = 2;
		
		pane.add(newNoteButton, newButtonGBConstraints, _NEW_BUTTON_INDEX);
		pane.add(searchCriteriaField, searchCritFieldGBConstraints, _SEARCH_CRITERIA_FIELD_INDEX);
		pane.add(searchButton, searchButtonGBConstraints, _SEARCH_BUTTON_INDEX);
		pane.add(searchResultPanel, contentAreaGBConstraints, _SEARCH_RESULT_PANEL_INDEX);
		pane.add(newNoteContentTextArea, contentAreaGBConstraints, _NEW_NOTE_CONTENT_INDEX);
		pane.add(saveButton, saveButtonGBConstraints, _SAVE_BUTTON_INDEX);
		pane.add(cancelButton, cancelButtonGBConstraints, _CANCEL_BUTTON_INDEX);
		pane.add(viewDoneButton, viewDoneButtonGBConstraints, _DONE_BUTTON_INDEX);

		
		newNoteContentTextArea.setVisible(false);
		saveButton.setVisible(false);
		cancelButton.setVisible(false);
		viewDoneButton.setVisible(false);
		searchResultPanel.setVisible(true);
		displayTodaysNotes(null);
		
		newNoteButton.addActionListener(new NewButtonListener(this));
		searchButton.addActionListener(new SearchButtonActionListener(this));
		saveButton.addActionListener(new SaveButtonActionListener(this));
		cancelButton.addActionListener(new CancelButtonActionListener(this));
		viewDoneButton.addActionListener(new ViewDoneButtonActionListener(this));
		newNoteContentTextArea.addMouseListener(new FieldClearingMouseListener());
		searchCriteriaField.addMouseListener(new FieldClearingMouseListener());
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	public void createAndShowGUI(boolean create) {
		// Create and set up the window.
		frame = new JFrame("My Diary");
		frame.setTitle("My Diary");
		frame.setSize(500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Set up the content pane.
		addComponentsToPane(frame.getContentPane());
		// Use the content pane's default BorderLayout. No need for
		// setLayout(new BorderLayout());
		// Display the window.
		//frame.pack();
		frame.setVisible(create);
	}



	public void update(PresentationModel pModel) {
		NotePresentationModel notePModel = (NotePresentationModel)pModel;
		frame.getContentPane().getComponent(_NEW_BUTTON_INDEX).setVisible(notePModel.isNewButtonShown());
		frame.getContentPane().getComponent(_SEARCH_CRITERIA_FIELD_INDEX).setVisible(notePModel.isSearchCriteriaShown());
		frame.getContentPane().getComponent(_SEARCH_BUTTON_INDEX).setVisible(notePModel.isSearchButtonShown());
		frame.getContentPane().getComponent(_NEW_NOTE_CONTENT_INDEX).setVisible(notePModel.isNoteContentShown());
		frame.getContentPane().getComponent(_SAVE_BUTTON_INDEX).setVisible(notePModel.isSaveButtonShown());
		frame.getContentPane().getComponent(_CANCEL_BUTTON_INDEX).setVisible(notePModel.isCancelButtonShown());
		frame.getContentPane().getComponent(_DONE_BUTTON_INDEX).setVisible(notePModel.isDoneButtonShown());
		
		if (notePModel.isNoteContentShown()) {
			((JTextArea)frame.getContentPane().getComponent(_NEW_NOTE_CONTENT_INDEX)).setText("Enter content");
			((JTextArea)frame.getContentPane().getComponent(_NEW_NOTE_CONTENT_INDEX)).revalidate();
		}
		
		if (notePModel.isSearchResultShown()) {
			createSearchResultTable(notePModel);
		} else {
			frame.getContentPane().getComponent(_SEARCH_RESULT_PANEL_INDEX).setVisible(false);
		}
		
		if (notePModel.isNoteViewShown()) {
			showSelectedNote(notePModel);
		}
		
	}
	
	private void displayTodaysNotes(NotePresentationModel notePModel) {
		if (notePModel == null) {
			Map<NoteViewData, String> viewMap = new HashMap<NoteViewData, String>();
			viewMap.put(NoteViewData.SEARCH_RESULT_SHOWN, null);
			try {
				_broker.updatePresentationModel(viewMap);
			} catch (DiaryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public String getNewNoteContent() {
		String noteContent = ((JTextArea)frame.getContentPane().getComponent(_NEW_NOTE_CONTENT_INDEX)).getText();
		return noteContent;
	}
	
	private void showSelectedNote(NotePresentationModel notePModel) {
		frame.getContentPane().getComponent(_SEARCH_RESULT_PANEL_INDEX).setVisible(true);
		String noteContent = notePModel.getNoteToView().get(NoteData.CONTENT);
	    GridBagConstraints noteViewGBConstraints = new GridBagConstraints();
	    noteViewGBConstraints.weightx = 0.5;
	    noteViewGBConstraints.weighty = 0.0;

	    noteViewGBConstraints.gridx = 0;
	    noteViewGBConstraints.gridy = 0;
	    noteViewGBConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
	    
	    JPanel panel = ((JPanel)frame.getContentPane().getComponent(_SEARCH_RESULT_PANEL_INDEX));
	    panel.removeAll();
	    panel.setLayout(new GridBagLayout());
	    JLabel headerLabel = new JLabel("<HTML><U>Note Content:</U></HTML>");
	    panel.add(headerLabel, noteViewGBConstraints);
	    noteViewGBConstraints.gridy = 1;
	    noteViewGBConstraints.weighty = 1.0;
	    noteViewGBConstraints.insets = new Insets(10, 0, 0, 0);
	    
	    JTextArea noteViewTextArea = new JTextArea(noteContent);
	    noteViewTextArea.setEditable(false);
	    noteViewTextArea.setBackground(null);
	    noteViewTextArea.setBorder(null);
	    panel.add(noteViewTextArea, noteViewGBConstraints);
	    panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}
	
	private void createSearchResultTable(NotePresentationModel notePModel) {
		
		List<Map<NoteData,String>> noteSearchResults = notePModel.getNoteResults();
		JTable searchResultTable = new JTable();
		String[] columnNames = {"id","What", "When"};
		String[][] values = new String[noteSearchResults.size()][3];
		for (int i=0; i<noteSearchResults.size(); i++) {
			Map<NoteData, String> note = noteSearchResults.get(i);
			String noteId = note.get(NoteData.ID);
			String noteCreationTime = note.get(NoteData.CREATION_TIME);
			String noteContent = note.get(NoteData.CONTENT);
			String[] value = {noteId, noteContent, noteCreationTime};
			values[i] = value;
		}
		
		searchResultTable.setModel(new DefaultTableModel(values, columnNames){
			private static final long serialVersionUID = 3636292002341793860L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		searchResultTable.addMouseListener(new ListDoubleClickListener(this));
		searchResultTable.setCellSelectionEnabled(false);
		searchResultTable.setRowSelectionAllowed(true);
	    JScrollPane searchResultScrollPane = new JScrollPane(searchResultTable);
	    GridBagConstraints searchResultGBConstraints = new GridBagConstraints();
	    searchResultGBConstraints.fill = GridBagConstraints.BOTH;
	    searchResultGBConstraints.ipady = 50; // make this component tall
	    searchResultGBConstraints.weightx = 1.0;
	    searchResultGBConstraints.weighty = 1.0;
	    searchResultGBConstraints.gridwidth = 1;
	    searchResultGBConstraints.gridx = 0;
	    searchResultGBConstraints.gridy = 0;
	    JPanel searchResultPanel = ((JPanel)frame.getContentPane().getComponent(_SEARCH_RESULT_PANEL_INDEX));	    
	    searchResultPanel.removeAll();
	    searchResultPanel.setLayout(new GridBagLayout());
	    searchResultPanel.add(searchResultScrollPane,searchResultGBConstraints);
	    searchResultPanel.revalidate();
	    searchResultPanel.repaint();
	    searchResultPanel.setVisible(true);
		searchResultTable.removeColumn(searchResultTable.getColumnModel().getColumn(0));

	}
	
	
	public static void main(String[] args) {
		//System.setProperty("apple.laf.useScreenMenuBar", "true");
		System.setProperty("com.apple.mrj.application.apple.menu.about.name", "My Diary");
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MyNotesUI myNotesUI = new MyNotesUI();
				myNotesUI.createAndShowGUI(true);
			}
		});
	}

	public String getSearchCriteria() {
		String searchCriteria = ((JTextField)frame.getContentPane().getComponent(_SEARCH_CRITERIA_FIELD_INDEX)).getText();
		return searchCriteria;
	}

}