package com.MyDiary.model.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.Note;
import com.MyDiary.model.factories.NoteFactory;

public class NoteTest {

	@Test(expected=DiaryException.class)
	public void testGetNoteDTO_forNull() throws DiaryException {
		Note note = new Note(null);
 	}
	
//	@Test
//	public void testGetNoteId_forBlank() {
//		Note note = new Note(null);
//		assertSame("Note id is blank", "", note.getNoteId());
//	}

//	@Test
//	public void testSave() throws DiaryException {
//		String noteContent = "Test Note Content";
//		Note note = NoteFactory.getNoteFactory().createNote(noteContent);
//		note.save();
//		String noteId = note.getNoteId();
//		
//		Note savedNote = note.getNote(noteId);
//		assertEquals("Note content not as expected", noteContent, savedNote.getContent());
//	}
	
	@Test
	public void testGetContent() throws DiaryException {
		String noteContent = "Test Note Content";
		Note note = NoteFactory.getNoteFactory().createNote(noteContent);
		String gotNoteContent = note.getContent();
		assertEquals("Note content not as expected", gotNoteContent, noteContent);
	}
	
//	@Test
//	public void testGetNote() throws DiaryException {
//		String noteContent = "Test Note Content";
//		Note note = new Note(noteContent);
//		
//		note.save();
//		String id = note.getNoteId();
//		Note gotNote = note.getNote(id);
//		String gotNoteContent = gotNote.getContent();
//		assertEquals("Note content not as expected", noteContent, gotNoteContent);
//		
//	}
	
}
