package com.MyDiary.model;

import com.MyDiary.datatransfer.NoteDTO;
import com.MyDiary.exceptions.DiaryException;


public class Note {

	private String _textContent = "";
	private String _id = "";
//	private String _systemDateFormat = "yyyy-MMM-dd' 'HH:mm:ss";
	private String _creationTime = "";
	
	
	
	public Note(NoteDTO noteDataObject) throws DiaryException {
		if (noteDataObject == null) {
			throw new DiaryException("Cannot initialize note. Null NoteDTO.");
		}
		this._id = noteDataObject.getId();
		this._textContent = noteDataObject.getContent();
		this._creationTime = noteDataObject.getCreationTime();
	}
	
	
	
//	public Note(String noteContent) {
////		_noteMetadata = generateMetadata();
//		_textContent = noteContent;
//		_notePersistor = NotePersistorFactory.getNotePersistor();
//	}
	
//	public Note(String noteId, String noteContent) {
//		_id = noteId;
//		_textContent = noteContent;
//	}
//	
//	public Note() {
//		_notePersistor = NotePersistorFactory.getNotePersistor();
//	}

//	private Metadata generateMetadata() {
//		Metadata metadata = new Metadata();
//		metadata.addMetadata("noteCreationTime", getTime());
//		metadata.addMetadata("noteCreationPlace", getPlace());
//		metadata.addMetadata("noteCreationMood", "Always happy");
//		return metadata;
//	}
//	
//	public void setMetadata(Metadata noteMetadata) {
//		this._noteMetadata = noteMetadata;
//	}
	
//	private String getTime() {
//		Date date = new Date();
//		SimpleDateFormat dateFormat = new SimpleDateFormat(_systemDateFormat);
//		String formattedDate = dateFormat.format(date);		
//		return formattedDate;
//	}
//	
//	private String getPlace() {
//		return "Pune";
//	}
	
	public String getContent() {
		return _textContent;
	}

//	public Note getNote(String noteId) throws DiaryException {
//		Note note = _notePersistor.getPersistedNote(noteId);
//		return note;
//	}
	
	public String getNoteId() {
		return this._id;
	}

//	public void save() throws DiaryException {
//		String id = _notePersistor.persistNote(this);
//		_id = id;
//	}
	
//	public String toString() {
//		StringBuilder values = new StringBuilder();
//		values
//			.append(getNoteId()).append(",")
//			.append(getContent()).append(",")
//			.append(_noteMetadata.getMetadata("noteCreationTime")).append(",")
//			.append(_noteMetadata.getMetadata("noteCreationPlace")).append(",")
//			.append(_noteMetadata.getMetadata("noteCreationMood"));
//		return values.toString();
//	}
//	public Map<String, String> getInfoMap() {
//		Map<String, String> infoMap = new HashMap<String, String>();
//		infoMap.put("noteId", getNoteId());
//		infoMap.put("noteTextContent", getContent());
//		infoMap.put("noteCreationTime", _noteMetadata.getMetadata("noteCreationTime"));
//		infoMap.put("noteCreationPlace", _noteMetadata.getMetadata("noteCreationPlace"));
//		infoMap.put("noteCreationMood", _noteMetadata.getMetadata("noteCreationMood"));
//		return infoMap;
//	}
	
	public NoteDTO getNoteDTO() throws DiaryException {
		if (_id == null) {
			return new NoteDTO(_textContent,_creationTime);
		} else {
			return new NoteDTO(_id, _textContent, _creationTime);
		}
	}

//	public List<Note> findNotes(NoteData noteData, String searchCriteria) throws DiaryException {
//		return _notePersistor.search(noteData, searchCriteria);
//	}

//	@Override
//	public int compareTo(Note inputNote) {
//
//		String inputNoteDate = null;//inputNote.getInfoMap().get("noteCreationTime");
//		String thisNoteDate = null;//this.getInfoMap().get("noteCreationTime");
//		DateFormat dateFormat = new SimpleDateFormat(_systemDateFormat);
//		int result = 0;
//		try {
//			result = dateFormat.parse(thisNoteDate).compareTo(dateFormat.parse(inputNoteDate));
//			if (result == 1) {
//				result = -1;
//			} else if (result == -1) {
//				result = 1;
//			}
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return result;
//	}
}
