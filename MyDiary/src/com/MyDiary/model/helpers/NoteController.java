package com.MyDiary.model.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.MyDiary.datatransfer.NoteDTO;
import com.MyDiary.datatransfer.factories.NoteRequestResponseFactory;
import com.MyDiary.datatransfer.interfaces.NoteRequest;
import com.MyDiary.datatransfer.interfaces.NoteResponse;
import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.infra.persistence.NotePersistorFactory;
import com.MyDiary.infra.persistence.interfaces.NotePersistor;
import com.MyDiary.model.Note;
import com.MyDiary.model.NoteData;
import com.MyDiary.model.factories.NoteFactory;

public class NoteController {
	
	private NotePersistor _notePersistor = null;
	
	private static NoteController _instance = new NoteController();
	private final NoteRequestResponseFactory _noteReqResFactory = NoteRequestResponseFactory.getInstance(); 
	
	private NoteController() {
		this._notePersistor = NotePersistorFactory.getNotePersistor();
	}
	
	public static NoteController getInstance() {
		return _instance;
	}

	public NoteResponse findNotes(NoteRequest request) throws DiaryException {
		NoteData criteriaName = (NoteData)request.getAttribute("searchCriteriaName");
		if (criteriaName == null) {
			throw new DiaryException("Null parameter for finding Notes.");
		}
		String criteriaValue = (String)request.getAttribute("searchCriteriaValue");
		List<NoteDTO> results =  _notePersistor.search(criteriaName, criteriaValue);
		List<Map<NoteData, String>> resultToUI = new ArrayList<Map<NoteData,String>>();
		for (NoteDTO noteDTO : results) {
			resultToUI.add(noteDTO.getAll());
		}
		
		NoteResponse response = _noteReqResFactory.getNoteResonse();
		
		
		response.setAttribute("searchResults", resultToUI);
		return response;
	}
	
	public NoteResponse getNote(NoteRequest request) throws DiaryException {
		NoteData criteriaName = (NoteData)request.getAttribute("searchCriteriaName");
		if (criteriaName == null) {
			throw new DiaryException("Null parameter for finding Notes.");
		}
		String criteriaValue = (String)request.getAttribute("searchCriteriaValue");
		
		NoteDTO noteDTO = _notePersistor.getPersistedNote(criteriaValue);
		Map<NoteData, String> result = new HashMap<NoteData, String>();
		if (noteDTO != null) {
			result = noteDTO.getAll();
		}
		NoteResponse response = _noteReqResFactory.getNoteResonse();
		response.setAttribute("searchResult", result);
		return response;
	}
	
	public String saveNote(NoteRequest request) throws DiaryException {
		String noteContent = (String)request.getAttribute(NoteData.CONTENT);
		if (noteContent == null || "".equals(noteContent)) {
			throw new DiaryException("No content to save.");
		}
		Note note = NoteFactory.getNoteFactory().createNote(noteContent);
		
		String id = _notePersistor.persistNote(note);
		return id;
	}
}
