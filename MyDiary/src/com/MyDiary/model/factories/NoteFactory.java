package com.MyDiary.model.factories;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.MyDiary.datatransfer.NoteDTO;
import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.Note;

public class NoteFactory {
	private final String _systemDateFormat = "yyyy-MMM-dd' 'HH:mm:ss";
	private static final NoteFactory _instance = new NoteFactory();
	private NoteFactory() {
		
	}

	public static NoteFactory getNoteFactory() {
		return _instance;
	}
	 
	public Note createNote(String content) throws DiaryException {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(_systemDateFormat);
		String formattedDate = dateFormat.format(date);	
		NoteDTO noteDTO = new NoteDTO(content, formattedDate);
		Note note = new Note(noteDTO);
		return note;
	}
}
