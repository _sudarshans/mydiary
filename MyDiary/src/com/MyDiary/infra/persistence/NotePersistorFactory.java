package com.MyDiary.infra.persistence;

import com.MyDiary.infra.persistence.interfaces.NotePersistor;

public class NotePersistorFactory {
	
	public static NotePersistor getNotePersistor() {
		return NoteSQLiteDBPersistor.getInstance();
	}
}
