package com.MyDiary.infra.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.MyDiary.datatransfer.NoteDTO;
import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.infra.persistence.interfaces.NotePersistor;
import com.MyDiary.model.Note;
import com.MyDiary.model.NoteData;

public class NoteSQLiteDBPersistor implements NotePersistor {

	private static NotePersistor _instance = new NoteSQLiteDBPersistor();
	private String _SQLiteDBLocation = "/Users/sudarshanshubakar/Documents/Sudarshan_Tech_work/diary/Database/diary";
	private String _tableName = "NewNote";
	private Statement _statement = null;
	
	private NoteSQLiteDBPersistor() {
		
	}
	@Override
	public String persistNote(Note note) throws DiaryException{
		if (note == null) {
			throw new DiaryException("NoteDTO object is null.");
		}
		NoteDTO noteDTO = note.getNoteDTO();
		String sql = createInsertSQL(noteDTO);
		String timeStamp = noteDTO.getCreationTime();
		String sqlForId = getSelectSQL(NoteData.CREATION_TIME, timeStamp);
		
		String id = null;
		try {
			updateDb(sql);
			ResultSet result = selectFromDB(sqlForId);
			result.next();
			id = result.getString(1);
			_statement.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		}
		return id;
	}

	@Override
	public NoteDTO getPersistedNote(String id) throws DiaryException {
		if (id == null || "".equals(id)) {
			throw new DiaryException("id is null or blank.");
		}
		String sql = createSelectSQL(id);
		ResultSet result = null;
		NoteDTO noteDTO = null;
		try {
			result = selectFromDB(sql);
			while(result.next()) {
				noteDTO = new NoteDTO(result.getString(1), result.getString(2),result.getString(3));
			}
			_statement.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		}
		return noteDTO;
	}
	
	private String createSelectSQL(String id) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("select * from "+ _tableName +" where noteid =\'").append(id).append("\'");
		return sqlB.toString();
	}

	public static NotePersistor getInstance() {
		return _instance;
	}

	private String createInsertSQL(NoteDTO noteDTO) {
		StringBuilder sqlB = new StringBuilder("insert into "+ _tableName +" (notetextcontent, notecreationtime) values(\'");
		sqlB.append(noteDTO.getContent()).append("\','")
			.append(noteDTO.getCreationTime()).append("\')");
//			
//		Map<String, String> noteInfoMap = note.getInfoMap();
//		sqlB.append("\'").append(noteInfoMap.get("noteTextContent")).append("\',");
//		sqlB.append("\'").append(noteInfoMap.get("noteCreationTime")).append("\',");
//		sqlB.append("\'").append(noteInfoMap.get("noteCreationPlace")).append("\',");
//		sqlB.append("\'").append(noteInfoMap.get("noteCreationMood")).append("\')");
		return sqlB.toString();
	}
	
	private void prepareStatement() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+_SQLiteDBLocation);
		_statement = connection.createStatement();
	}
	
	private void updateDb(String sql) throws ClassNotFoundException, SQLException {
//		Statement statement = getStatement();
		prepareStatement();
		_statement.executeUpdate(sql);
		_statement.close();
	}
	
	private ResultSet selectFromDB(String sql) throws SQLException, ClassNotFoundException {
//		Statement statement = getStatement();
		prepareStatement();
		ResultSet resultSet = _statement.executeQuery(sql);
//		statement.close();
		return resultSet;
	}

	@Override
	public List<NoteDTO> search(NoteData noteData, String searchCriteria) throws DiaryException {
		String sql = getSelectSQL(noteData, searchCriteria);
		List<NoteDTO> searchResult = new ArrayList<NoteDTO>();
		try {
			ResultSet result = selectFromDB(sql);
			while(result.next()) {
				//Map<String, Note> row = new HashMap<String, Note>();
				NoteDTO noteDTO = new NoteDTO(result.getString(1), result.getString(2), result.getString(3));
//				Note note = new Note(result.getString(1), result.getString(2));
//				Metadata noteMetadata = new Metadata();
//				noteMetadata.addMetadata("noteCreationTime", result.getString(3));
//				noteMetadata.addMetadata("noteCreationPlace", result.getString(4));
//				noteMetadata.addMetadata("noteCreationMood", result.getString(5));
//				note.setMetadata(noteMetadata);
				//row.put(note.getNoteId(), note);
				searchResult.add(noteDTO);
			}
			_statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new DiaryException(e.getLocalizedMessage());
		}
		Collections.sort(searchResult);
		return searchResult;
	}

	private String getSelectSQL(NoteData noteData, String searchCriteria) {
		//String columnName = "";
		List<String> columnList = new ArrayList<String>();
		if (noteData == NoteData.CONTENT) {
			columnList.add("notetextcontent");
		} else if(noteData == NoteData.CREATION_TIME) {
			columnList.add("notecreationtime");
		} else if (noteData == NoteData.ALL) {
			columnList.add("notetextcontent");
			columnList.add("notecreationtime");
		}
		
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("select * from "+ _tableName +" where ");
		Iterator<String> columnIterator = columnList.iterator();
		while (columnIterator.hasNext()) {
			sqlB.append(columnIterator.next())
			.append(" like \'%")
			.append(searchCriteria)
			.append("%\'");
			if (columnIterator.hasNext()) {
				sqlB.append(" OR ");
			}
		}

		return sqlB.toString();
	}
	
}
