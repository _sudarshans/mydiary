package com.MyDiary.infra.persistence.interfaces;

import java.util.List;

import com.MyDiary.datatransfer.NoteDTO;
import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.model.Note;
import com.MyDiary.model.NoteData;

public interface NotePersistor {

	String persistNote(Note note) throws DiaryException;
	
	NoteDTO getPersistedNote(String id) throws DiaryException;

	List<NoteDTO> search(NoteData noteData, String searchCriteria) throws DiaryException;
}
