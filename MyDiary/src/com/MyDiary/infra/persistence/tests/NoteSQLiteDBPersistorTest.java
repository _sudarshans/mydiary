package com.MyDiary.infra.persistence.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.MyDiary.exceptions.DiaryException;
import com.MyDiary.infra.persistence.NotePersistorFactory;
import com.MyDiary.infra.persistence.interfaces.NotePersistor;
import com.MyDiary.model.Note;
import com.MyDiary.model.factories.NoteFactory;

public class NoteSQLiteDBPersistorTest {

	private NotePersistor notepersistor = NotePersistorFactory.getNotePersistor();
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPersistNote() throws DiaryException {
//		fail("Not yet implemented");
		Note note = NoteFactory.getNoteFactory().createNote("TestNoteContent");
		//String id = note.getNoteId();
		String id = notepersistor.persistNote(note);
		String persistedID = notepersistor.getPersistedNote(id).getId();
		assertNotSame("Incorrect id for persisted note", id, persistedID);
	}
	
	@Test(expected=DiaryException.class)
	public void testPersistNote_Null() throws DiaryException {
		notepersistor.persistNote(null);
	}

	@Test
	public void testGetPersistedNote() throws DiaryException {
		Note note = NoteFactory.getNoteFactory().createNote("TestNoteContent");
		String id = notepersistor.persistNote(note);
//		note.getNoteId();

		String persistedID = notepersistor.getPersistedNote(id).getId();
		assertNotSame("Incorrect id for persisted note", id, persistedID);	
	}
	
	@Test(expected=DiaryException.class)
	public void testGetPersistedNote_Null() throws DiaryException {
		notepersistor.getPersistedNote(null);
	}

//	@Test
//	public void testGetInstance() {
//		fail("Not yet implemented");
//	}

}
